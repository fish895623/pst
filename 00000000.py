import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
import platform
import __deletecomma as dc

if platform.system() == 'Windows':
    # 윈도우인 경우
    font_name = font_manager.FontProperties(
        fname="c:/Windows/Fonts/malgun.ttf").get_name()
    rc('font', family=font_name)
else:
    # Mac 인 경우
    rc('font', family='AppleGothic')

matplotlib.rcParams['axes.unicode_minus'] = False

dc.deletesub("original.csv", "out.csv")

sales = pd.read_csv("out.csv", encoding='euc-kr')

print(sales.head())

customer_group = sales.groupby('광역지자체')
sales_totals = customer_group.sum()
my_plot = sales_totals.plot(kind='bar', )
plt.show()
