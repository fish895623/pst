import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
import platform

# 폰트 설정
if platform.system() == 'Windows':
    # 윈도우인 경우
    font_name = font_manager.FontProperties(
        fname="c:/Windows/Fonts/malgun.ttf").get_name()
    rc('font', family=font_name)
else:
    # Mac 인 경우
    rc('font', family='AppleGothic')
# 마이너스 표시설정
matplotlib.rcParams['axes.unicode_minus'] = False


df = pd.read_csv("b-1.csv", engine='python', encoding='utf-8')

if ',' in df.iloc[0, 1]:
    a = df.iloc[0, 1].replace(',', '')
    # df.iloc[0, 1] = int(df.iloc[0, 1])
else:
    print("don't have to change")

print(a)

# print(df.iloc[0, 3].type)
'''
city_group = df.groupby('광역지자체')
city_total = city_group.sum()
my_plot = city_total.plot(kind='bar')
plt.show()
'''
