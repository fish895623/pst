# csv 파일을 tsv 파일로 변환
import csv

__csv = 'b-1.csv'
__tsv = 'b-1.tsv'
__csv_bak = 'b-1.bak.csv'

with open('b-1.csv', 'r', encoding='utf-8') as csvin, open('new.tsv', 'w') as tsvout:
    csvin = csv.reader(csvin)
    tsvout = csv.writer(tsvout, delimiter='\t')

    for row in csvin:
        tsvout.writerow(row)
