#include <stdio.h>
#include <malloc.h>

struct tfield
{
    char name[20];
    struct tfield *pointer;
};

struct tfield *talloc()
{
    return (struct tfield *)malloc(sizeof(struct tfield));
};

struct tfield *head;
struct tfield *p;

void addnode()
{
    while (scanf("%s", p->name) != EOF)
    {
        p->pointer = head;
        head = p;
        p = talloc();
    }
}

void main()
{
    struct tfield *head, *p;
    head = NULL;
    p = talloc();
    printf("input\n");
    while (scanf("%s", p->name) != EOF)
    {
        p->pointer = head;
        head = p;
        p = talloc();
    }
    p = head;
    while (p != NULL)
    {
        printf("%s\n", p->name);
        p = p->pointer;
    }
}
