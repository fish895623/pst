#include <stdio.h>

struct student
{
    int id;
    double score;
};


void main()
{
    struct student s[3] = {{1, 98.9}, {2, 80.5}, {3, 78.5}}, *p;
    int i;
    p = s;
    for (i = 0; i < 3; i++)
    {
        printf("id = %d, score %lf\n", p->id, p->score);
        p++;
    }
}