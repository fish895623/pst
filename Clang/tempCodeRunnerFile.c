#include <stdio.h>
#include <malloc.h>
#include <string.h>
struct tfield
{
    char name[20];
    struct tfield *pointer; /* 다음데이터를가리키는포인터*/
};
struct tfield *talloc(); // 함수선언
struct tfield *head;
void add(char *newn)
{
    struct tfield *p;
    p = talloc();      // 새로운 노드 생성
    p->pointer = head; // 새로운 노드가 리스트의 첫 노드를 가리키기
    head = p;          // 새로운 노드를 head가 가리키기
    strcpy(p->name, newn);
}
void main()
{
    struct tfield *p;
    char newname[20];
    head = NULL;
    p = talloc();
    printf("사람들의이름을원하는만큼입력한후^Z를입력하시오.\n");
    while (scanf("%s", p->name) != EOF)
    {
        p->pointer = head; // head의값이아니라주소값이
        head = p;
        p = talloc();
    }
    p = head;
    while (p != NULL)
    {
        printf("%s\n", p->name);
        p = p->pointer;
    }
    printf("추가할사람의이름을입력하시오.\n");
    scanf("%s", newname);
    add(newname);
    printf("추가한 결과:\n");
    p = head;
    while (p != NULL)
    {
        printf("%s\n", p->name);
        p = p->pointer;
    }
    system("pause");
}
struct tfield *talloc()
{
    return (struct tfield *)malloc(sizeof(struct tfield));
}