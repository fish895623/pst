import csv


def deletesub(__csvin, __csvout):
    __tsvout = 'csv.bak'
    with open(file=__csvin, mode='r', encoding='utf-8') as csvin,\
            open(file=__tsvout, mode='w', encoding='euc-kr') as tsvout:
        csvin = csv.reader(csvin)
        tsvout = csv.writer(tsvout, delimiter='\t')

        for row in csvin:
            tsvout.writerow(row)

    with open(file=__tsvout, mode='r', encoding='euc-kr') as text, \
            open(file=__csvout, mode='w', encoding='euc-kr') as bakout:

        while True:
            line = text.readline()

            line = line.replace(',', '')
            line = line.replace('\t', ',')

            if not line:
                break
            bakout.write(line)
