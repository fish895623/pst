#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void repeat(int size, int *numPtr)
{
	int i, k, a = 0;
	for (i = 0; i < size; i++)
	{
		printf("input :");
		scanf("%d", &k);
		a++;
		if (numPtr[i] == k)
		{
			printf("RIGHT %d\n", numPtr[i]);
		}
		else if (numPtr[i] != k)
		{
			printf("WRONG %d\n", numPtr[i]);
			repeat(size, numPtr);
		}
		printf("%d", a);
	}
}

void repeata(int size, int *numPtr)
{
	int i = 0, k;

	while (i < 5)
	{
		scanf("%d", &k);
		i++;
		printf("RIGHT %d\n", numPtr[i]);
	}
}

void main()
{
	int size, i;

	srand((unsigned int)time(NULL));

	printf("PUT SIZE NUM : ");
	scanf("%d", &size);
	int *numPtr = malloc(sizeof(int) * size);

	for (i = 0; i < size; i++)
	{
		numPtr[i] = rand() % 5;
	}

	for (i = 0; i < size; i++)
	{
		printf("%d\n", numPtr[i]);
	}

	// repeat(size, numPtr);

	repeata(size, numPtr);

	free(numPtr);
}
