#ifndef __NODE_H__
#define __NODE_H__
typedef struct tfield
{
    /* data */
    struct tfield *left;
    struct tfield *right;
    int name;
}NODE;
struct tfield *talloc(){
    return (struct tfield *)malloc(sizeof(struct tfield));
}
#endif  //__NODE_H__