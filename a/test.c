#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void repeata(int size, int *numPtr)
{
	int i = 0, k;

	while (i < size)
	{
		scanf("%d", &k);
		printf("RIGHT %d\n", numPtr[i]);
		i++;
	}
}

void main()
{
    int i, size;
	srand((unsigned int)time(NULL));

    // scanf("%d", &size);

	size = 3;

    int *numPtr = malloc(sizeof(int) * size);

	for (i = 0; i < size; i++)
	{
		numPtr[i] = rand() % 5;
		printf("%d\n", numPtr[i]);
	}

    repeata(size, numPtr);

    free(numPtr);
}