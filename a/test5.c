#include <stdio.h>
#include <malloc.h>
#include <time.h>
#include <string.h>

#include "node.h"

void main()
{
    srand((unsigned int)time(NULL));
    NODE *p;
    NODE *head;

    head = NULL;
    p = talloc();

    int i = 0;

    do
    {
        p->name = rand() % 99;
        if (head == NULL)
        {
            p->left = head;
            p->right = head;
            head = p;
        }
        else
        {
            p->left = head->left;
            p->right = head;
            head->left = p;
            head = p;
        }
        p = talloc();
        i++;
    } while (i <= 9);

    p = head;
    while (p->right != NULL)
    {
        printf("%d\n", p->name);
        p = p->right;
    }
    printf("%d\n\n", p->name);
}