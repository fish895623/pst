#include <stdio.h>
#include <malloc.h>
#include <time.h>
#include <string.h>

#include "node.h"

void main()
{
	NODE one;
	NODE two;
	NODE three;
	NODE *p;
	srand((unsigned int)time(NULL));

	one.name = rand() % 3;
	two.name = rand() % 3;
	three.name = rand() % 3;

	one.right = &two;
	two.right = &three;
	three.right = NULL;

	printf("%d\n", one.name);
	printf("%d\n", two.name);
	printf("%d\n", three.name);
	printf("\n");

	p = &one;

	while (NULL != p)
	{
		printf("%d\n", p->name);
		p = p->right;
	}
	printf("\n");
}