#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>

struct tfield
{
    struct tfield *left;
    int name;
    struct tfield *right;
};
struct tfield *talloc();

void main()
{
    int i = 0;
    struct tfield *head, *p;
    head = NULL;
    p = talloc();
    srand((unsigned int)time(NULL));

    printf("사람들의이름을원하는만큼입력한후^Z를입력하시오.\n");

    do
    {
        p->name = rand() % 99;
        if (head == NULL)
        {
            p->left = head;
            p->right = head;
            head = p;
        }
        else
        {
            p->left = head->left;
            p->right = head;
            head->left = p;
            head = p;
        }
        p = talloc();
        i++;
    } while (i == 9);

    p = head;
    while (p->right != NULL)
    {
        printf("%d\n", p->name);
        p = p->right;
    }
    printf("%d\n", p->name);
}
struct tfield *talloc()
{
    return (struct tfield *)malloc(sizeof(struct tfield));
}